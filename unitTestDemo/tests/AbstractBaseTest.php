<?php
namespace Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Created by PhpStorm.
 * User: demo
 * Date: 5/12/18
 * Time: 11:37
 */
abstract class AbstractBaseTest extends WebTestCase
{

    protected KernelBrowser $client;

    public function setUp(): void {

        $this->client = static::createClient();

        $kernel = $this->bootKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'doctrine:database:create'
        ));

        $output = new BufferedOutput();
        $application->run($input, $output);
        
        $input = new ArrayInput(array(
            'command' => 'doctrine:schema:update',
            "--force" => true
            
        ));

        $application->run($input, $output);
    }
}
