<?php
namespace Tests\App\Unit;


use App\Service\LowerOrGreater;
use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
/**
 * Created by PhpStorm.
 * User: jona
 * Date: 16/10/18
 * Time: 16:06
 */
class LowerOrGreaterUnitTest extends WebTestCase
{

    /**
     * @var LowerOrGreater
     */
    private $lowerOrGreater;
    
    
    /**
     * @before
     */
    public function init(){
        $client = static::createClient();
        $this->lowerOrGreater = $client->getContainer()->get(LowerOrGreater::class);
    }

    public function testExceptionsNull(){
        $this->expectException(\InvalidArgumentException::class);
        $this->lowerOrGreater->isGreaterThan(null,null);

    }

    public function testExceptionsString(){
        $this->expectException(\InvalidArgumentException::class);
        $this->lowerOrGreater->isGreaterThan(100,"Ceci n'est pas un numeric");

    }
    
    public function testGreater(){

        $this->assertTrue($this->lowerOrGreater->isGreaterThan(100,1));
        $this->assertFalse($this->lowerOrGreater->isGreaterThan(100,1000));
        $this->assertFalse($this->lowerOrGreater->isGreaterThan(1000,1000));
        $this->assertFalse($this->lowerOrGreater->isGreaterThan(-1,1000));
    }
    
    public function testLower(){
        $this->assertTrue($this->lowerOrGreater->isLowerThan(100,1000000));
        $this->assertTrue($this->lowerOrGreater->isLowerThan(100,1000));
        $this->assertFalse($this->lowerOrGreater->isLowerThan(1000,1000));
        $this->assertTrue($this->lowerOrGreater->isLowerThan(-1,1000));
    }
    
    

}
