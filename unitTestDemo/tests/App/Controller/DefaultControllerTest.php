<?php

namespace Tests\App\Controller;

use App\Entity\Post;
use Peekmo\JsonPath\JsonStore;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Tests\AbstractBaseTest;

//Tests d'intégration
class DefaultControllerTest extends AbstractBaseTest
{
    
    const POST_TITLE = "post title";
    const POST_CONTENT = "post content leirfn rlsfsnh sij sjfs ";

    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
    
    
    public function testCreatePost()
    {
        $postData = ["title"=>self::POST_TITLE, "content"=>self::POST_CONTENT];
        $this->client->catchExceptions(false); //CEtte ligne affiche les erreurs dans PHPunit au lieu du fichier de logs
        $this->client->request('POST', '/posts',[],[],[],json_encode($postData));
        
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("application/json", $this->client->getResponse()->headers->get('Content-Type'));

        $postFromResponse = self::getPostFromJson($this->client->getResponse()->getContent());
        $this->assertNotNull($postFromResponse->getId());
        $this->assertEquals($postFromResponse->getTitle(),self::POST_TITLE);
        $this->assertEquals($postFromResponse->getContent(),self::POST_CONTENT);

        $postFromDB = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Post::class)->find($postFromResponse->getId());

        $this->assertEquals($postFromDB->getTitle(),self::POST_TITLE);
        $this->assertEquals($postFromDB->getContent(),self::POST_CONTENT);
            
            
    }
    
    public function testCreatePostContentLength()
    {

        $postData = ["title"=>self::POST_TITLE,"content"=>"AAA"];
        $this->client->catchExceptions(false); //CEtte ligne affiche les erreurs dans PHPunit au lieu du fichier de logs
        $this->client->request('POST', '/posts',[],[],[],json_encode($postData));
        
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        
    }
    
    public function testGetPost()
    {

        $post = new Post();
        $post->setTitle(self::POST_TITLE);
        $post->setContent(self::POST_CONTENT);
        
        $em = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $em->persist($post);
        $em->flush();
        
        $this->client->catchExceptions(false); //CEtte ligne affiche les erreurs dans PHPunit au lieu du fichier de logs
        $this->client->request('GET', '/posts/'.$post->getId());

        $testPost = self::getPostFromJson($this->client->getResponse()->getContent());
        $this->assertEquals($testPost->getId(),$post->getId());
        $this->assertEquals($testPost->getContent(),$post->getContent());
        $this->assertEquals($testPost->getTitle(),$post->getTitle());
        
    }
    
    public function testGetPostNotFound()
    {
        $post = new Post();
        $post->setTitle(self::POST_TITLE);
        $post->setContent(self::POST_CONTENT);
        
        $em = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $em->persist($post);
        $em->flush();
        
        $this->client->request('GET', '/posts/'.($post->getId()+1));
        
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        
    }

    private static function getPostFromJson(string $json): Post {
        $encoders = array( new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $post = $serializer->deserialize($json,Post::class,"json");

        return $post;
    }
    
    
}
