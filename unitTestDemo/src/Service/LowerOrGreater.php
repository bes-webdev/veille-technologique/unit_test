<?php
namespace App\Service;
/**
  * User: jona
 * Date: 16/10/18
 * Time: 15:54
 */
class LowerOrGreater
{
    
    public function isLowerThan($testable,$limit){
        return $testable<$limit;
    }
    
    public function isGreaterThan($testable,$limit){
        if ($testable === null || $limit === null){
            return false;
        }
        return $testable>$limit;
    }

}
