<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param $id
     * @Route("/posts/{id}",name="getPost",methods={"GET"})
     * @return JsonResponse
     */
    public function getPostAction(Post $post){

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        
        return new Response($serializer->serialize($post,"json"),200,['Content-Type'=>"application/json"]);
    }
    
    /**
     * @Route("/posts",name="createPost",methods={"POST"})
     */
    public function createAction(Request $request, ValidatorInterface $validator, EntityManagerInterface $em){
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $post = $serializer->deserialize($request->getContent(),Post::class,"json");
        
        
        $errors = $validator->validate($post);
        if (count($errors)){
            return new JsonResponse($errors,400);
        }
        
        $em->persist($post);
        $em->flush();
        
        return new Response($serializer->serialize($post,"json"),201,["Content-Type"=>"application/json"]);
    }
}
