# Tester son code
L’objet de cette leçon est une initation aux tests informatiques.

Tout développer se doit de tester son code. Ca permet de valider le code produit par rapport à des spécifications données. Ca permet aussi et principalement d’éviter la régression (en modifiant mon code, j’ai créé un bug quelque part). 

Il existe des outils pour ça. En PHP, l’outil recommandé s’appelle PHPUnit. Nous allons voir comment l’installer et comment l’utiliser.


## Installation 
PHPUnit s’installe avec composer. Il peut s’installer par projet ou globalement. Pour ma part l’utilisant tout le temps je vais privilégier une installation globale.
Pour l’installer, rendez-vous dans la console.

Par projet : 
1. Déplacez-vous dans le dossier de votre projet et lancez la commande `composer require phpunit/phpunit`.
2. Ha ben il n’y a pas de deux. C’est fini:) .

Pour l’installer globalement, ajoutez global à votre commande : 
1. `composer global require phpunit/phpunit`
2. ajoutez le chemin de phpunit au path (linux : lancez la commande  ou `export PATH=~/.config/composer/vendor/bin:$PATH` selon le chemin de l'installation , pour windows: ajoutez le chemin au path des variables systèmes dans le panneau de configuration).
3. Pour mettre à jour une dépendance globale, lancez la commande `composer global update`

Testez l’installation en lancant la commande `phpunit -v`.

## Types de tests
En réalité il y a plusieurs moyens de tester son code, et il y a plusieurs types de tests qui ont de objectifs et des cadres d’utilisation différents. Le principe va rester globalement le même dans tous les cas : on va executer du code, et on va vérifier (supposer) que le résultat est bien celui qu’on attend. On va appeler ça des assertions. Nous allons nous concentrer sur deux types de tests. Ces deux types de tests sont complémentaires, ce n’est pas l’un ou l’autre. C’est l’un et l’autre.
Illustration: https://lesjoiesducode.fr/quand-les-tests-unitaires-sont-au-vert-et-quil-ny-a-pas-eu-de-test-dintegration?fbclid=IwAR3m2SHKGS7C8rokijB2sJA5bOS3zbwr90PqQNORa5Wb4whGGgM875ki1-c
### Tests Unitaires 
Le test unitaire est une technique qui sert à tester des bouts de code (une classe, une methode,…) indépendamment du reste du code. On extrait une partie du code et on vérifie qu’elle fait bien ce qu’elle est sensée faire.
Par exemple si on a une classe Calculatrice avec une methode multiply, on va vérifier que quand on donne 2 et 2 à multiply, multiply nous renvoie bien 4. Pour bien faire on va aussi vérifier les cas spéciaux : Si je fourni un 0 le retour doit être 0, je vérifie aussi avec les signes + et -. On va aussi vérifier qu'une erreur est bien envoyée quand on envoie des valeurs bizarres genre des string, ou autre chose d'inattendu
### Tests d’intégration
Le test d’intégration va tester l’ensemble des composants de notre application. On va tester le résultat de l’assemblage (intégration) des différents modules. 
Par exemple, on pourrait tester qu’une requete get sur une ressource inexistante nous renvoie bien une réponse avec un code HTTP 404. On va tester qu’une requête POST d’un object non valide va nous retourner une 400. On peut (doit) aussi tester qu’une requete post qui reçoit une réponse 201 a bien persisté la ressource en DB,...

## Quelques derniers concepts bien utiles
## Mock
Les mocks sont la simulation d’un service, d’un object (base de données, service web,..). Ils sont très utiles. On peut en « moquant » un Repository tester le comportement d’un objet pour une réponse précise de la DB. On peut provoquer des exceptions pour tester le comportement face à l’exception,… 

**Attention de ne pas trop « moquer » car chaque fois qu’un object ou un service est moqué, il n’est pas testé en lui même et on pourrait se retrouver avec des tests qui ne reflètent pas la réalité de l’application**.
## Test driven development 
Le test driven development est un concept dans lequel face à une fonctionalité ou un module a développer, on commence par écrire les tests correspondants. Ces tests vont d’office échouer étant donné que le code n’est pas encre écrit. Il faut ensuite écrire le code jusqu’à ce que le tests fonctionne. 



 
